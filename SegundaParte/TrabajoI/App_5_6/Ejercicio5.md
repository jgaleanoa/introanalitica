---
title: ''
---

## Método indirecto

### Mejor subconjunto

Según las métricas $R_{\text{Adj}}^2, \ C_p \text{ de Mallows y } BIC$ los mejores
modelos son aquellos que contienen 4 o 5 covariables.

* bcs, pindex, enzyme_test, liver_test y alc_heavy según $R^2$ **ajustado**.
* bcs, pindex, enzyme_test y alc_heavy según el $C_p$ de Mallows y el BIC.

### Selección hacia adelante

En esta situación, $R^2_{\text{Adj}}, \ C_p \text{ de Mallows y BIC}$ llegan a 
un consenso considerando el mejor modelo aquel con 5 covariables las cuales son
bcs, pindex, enezyme_test, liver_test y alc_heavy.

### Selección hacia atrás

Las mejores variables según este método son las mismas que las del mejor subconjunto.

* bcs, pindex, enezyme_test, liver_test y alc_heavy según  $R^2$ **ajustado**.
* bcs, pindex, enezyme_test, y alc_heavy según $C_p$ de Mallows y el BIC.


## Método directo

### Train - Test Split

Según MSE los mejores modelos usando los métodos del mejor subconjunto,
selección adelante y hacia atrás respectivamente son aquellos con 8 y 6 
covariables (mejor subconjunto y selección llegan al mismo resultado), 
sin embargo, se puede notar que a partir de los modelos de 4 covariables 
la disminución de este es mínimo y se pueden considerar mejores al ser más
parsimoniosos.

## K - Fold Cross Validation

Se hace la aclaración de que se usaron 3 capas. El método de mejor subconjunto 
y el de selección hacia atrás llegan a que se deben usar 4 covariables al 
resultar en un menor MSE mientras que el método de selección hacia adelante 
considera mejor el de 5 covariables.

## Leave One Out Cross Validation

Dada la escacez de observaciones, esta metodología de cross validation es la más
fiable de todas.En este caso, todos los métodos llegan a que el modelo con menor
MSE es aquel que usa 5 covariables.

## Conclusión final

Después de analizar el desempeño de los modelos usando métricas indirectas 
(como $R_{\text{Adj}}^2, \ C_p, \ BIC$) y directas (MSE) se concluye que el 
número óptimo de covariables a usar es 5 pues en la métrica directa más 
adecuada puesto que, para la situación presentada (LOOCV), los modelos con 
este número de covariables fueron los que obtuvieron un menor MSE. Por otro 
lado, según las métricas indirectas el número adecuado de covariables es 4 o 5 
lo cual es coherente con lo mencionado anteriormente. Finalmente se escoge el 
modelo de covariables que tuvo un menor MSE, dicho modelo usó las covariables 
bcs, pindex,enzyme_test, liver_test y alc_heavy, las cuales fueron detectadas
previamente como las más importantes gracias a los métodos indirectos.


